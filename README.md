This project has moved
======================

See https://gitlab.com/european-language-grid/platform/i18n-service - this repository is preserved only for its existing container registry images.
